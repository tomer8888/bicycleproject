﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BicycleProject.Models
{
    public class Clip
    {
        public string name { get; set; }
        public int id { get; set; }
        public TimeSpan timeSpan { get; set; }
        public string path { get; set; }
        public List<ClipElements> elements { get; set; }

        public Clip()
        {
            elements = new List<ClipElements>();
        }
    }

    public class User
    {
        public string id { get; set; }
        public UserType type { get; set; }
    }

    public enum UserType {
        A,
        B,
    }

    public class ClipElements
    {
        public int qouter { get; set; }
        public List<ClipElementsEnum> elements { get; set; }

        //public ClipElements() {
        //    elements = new List<ClipElementsEnum>();
        //}
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("Q: " + qouter + ",");
            if (elements.Count > 0)
            {
                // add selected elemets
                foreach (var el in elements)
                {
                    sb.AppendFormat("{0},", el.ToString());
                }
            }
            return sb.ToString();
        }
    }

    public enum ClipElementsEnum
    {
        Truck,
        Car,
        Bike,
        Bicycle,
        Bus,
        Van


    }

}

﻿using BicycleProject.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BicycleProject.Controllers
{
    class VideoPlayerController
    {
        public User user { get; set; }
        public bool Initiated = false;
        private List<Clip> clipList;
        private int clipIndex = 0;

        private static VideoPlayerController instance;
        public static VideoPlayerController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VideoPlayerController();
                }
                return instance;
            }
        }

        // constructor
        private VideoPlayerController()
        {
            clipList = new List<Clip>();
        }

        public void loadClipsFromPath(string path)
        {
            // clear old clips
            clipList.Clear();
            try
            {
                DirectoryInfo directory = new DirectoryInfo(path);

                FileInfo[] files = new string[] { "*.mp4", "*.wmv" }
                        .SelectMany(i => directory.GetFiles(i, SearchOption.TopDirectoryOnly))
                        .Distinct().ToArray();

                var index = 0;
                foreach (var file in files)
                {
                    var clip = new Clip()
                    {
                        name = file.Name,
                        id = index++,
                        path = file.FullName,
                    };
                    clipList.Add(clip);
                }

                // shuffleList!
                clipList = getShuffleList();

                Initiated = true;
            }
            catch (Exception ex)
            {
                Initiated = false;
            }
        }
        
        public void resetList()
        {
            clipIndex = 0;
        }

        public Clip getNextClip()
        {
            if (clipIndex < clipList.Count)
            {
                return clipList[++clipIndex % clipList.Count];
            }
            else
            {
                // print to file
                logUser();
                return null;
            }
            
        }

        // Shuffle clip list
        public List<Clip> getShuffleList()
        {
            //return clipList.OrderBy(a => Guid.NewGuid()).ToList();
            return clipList.OrderBy(a => Guid.NewGuid()).ToList();
        }

        private void logUser()
        {
            var sb = new StringBuilder();

            sb.AppendFormat("{0},type: {1},",user.id,user.type);
            foreach (var c in clipList)
            {
                sb.AppendLine();// new line for every clip
                sb.AppendFormat("{0},{1},", c.name, c.timeSpan.TotalMilliseconds);
                if (c.elements.Count > 0)
                {
                    // add selected elemets
                    foreach(var el in c.elements)
                    {
                        sb.AppendFormat("{0},", el.ToString());
                    }
                }
            }
            sb.AppendLine();
            File.AppendAllText("log.csv", sb.ToString());
        }

    }
}

﻿using BicycleProject.Controllers;
using BicycleProject.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace BicycleProject
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        VideoPlayerController videoPlayerController = VideoPlayerController.Instance;

        private void appLoaded(object sender, StartupEventArgs e)
        {
            // init user
            videoPlayerController.user = new User();

            // Application is running
            // Process command line args
            for (var i = 0; i != e.Args.Length; i++)
            {
                if (e.Args[i] == "/A")
                {
                    videoPlayerController.user.type = UserType.A;
                    //loadClipsFromDir(path + @"\MediaA\");
                }
                else if (e.Args[i] == "/B")
                {
                    videoPlayerController.user.type = UserType.B;
                    //loadClipsFromDir(path + @"\MediaB\");
                }
                
            }

            loadClipsFromDir(@"\MediaTest\");

            // in case no arguments
            if (!videoPlayerController.Initiated)
            {
                videoPlayerController.user.type = UserType.B;
                //loadClipsFromDir(path + @"\MediaTest\");
            }
        }

        private void loadClipsFromDir(string path)
        {
            // Process open file dialog box results  
            var localPath = System.IO.Directory.GetCurrentDirectory();

            // load files
            videoPlayerController.loadClipsFromPath(localPath+path);       
        }

    }

    
}

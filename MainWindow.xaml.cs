﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BicycleProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Welcome.userSelectedEventHandler += userSelectedEvent;
            Player.playerFinishEventHandler += playerFinishEvent;
            Player.playerFinishTestRound += playerFinishTestRound;

            TestRound.userContinueEventHandler += userSelectedEvent;
            
        }

        private void playerFinishTestRound(object sender, EventArgs e)
        {
            showUserControlByName(UserControlsEnum.TestRound);
        }

        private void userSelectedEvent(object sender, EventArgs e)
        {
            showUserControlByName(UserControlsEnum.Player);
        }

        private void playerFinishEvent(object sender, EventArgs e)
        {
            showUserControlByName(UserControlsEnum.Finish);
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            showUserControlByName(UserControlsEnum.Welcome);

            // Set DataContext
            //this.lblVersion.DataContext = this;
        }

        private void hideAllUserControls()
        {
            Player.Visibility = Visibility.Hidden;
            Welcome.Visibility = Visibility.Hidden;
            Finish.Visibility = Visibility.Hidden;
            TestRound.Visibility = Visibility.Hidden;
        }

        public void showUserControlByName(UserControlsEnum obj)
        {
            hideAllUserControls();
            switch (obj)
            {
                case UserControlsEnum.Player:
                    Player.Visibility = Visibility.Visible;
                    Player.checkVideoPlayerInit();
                    break;
                case UserControlsEnum.Welcome:
                    Welcome.Visibility = Visibility.Visible;
                    break;
                case UserControlsEnum.Finish:
                    Finish.Visibility = Visibility.Visible;
                    break;
                case UserControlsEnum.TestRound:
                    TestRound.Visibility = Visibility.Visible;
                    break;
            }
        }

        public enum UserControlsEnum
        {
            Player,
            Welcome,
            Finish,
            TestRound,
        }
    }


}

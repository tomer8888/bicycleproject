﻿using BicycleProject.Controllers;
using BicycleProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BicycleProject.UserControls
{
    /// <summary>
    /// Interaction logic for WelcomeUC.xaml
    /// </summary>
    public partial class WelcomeUC: UserControl
    {
        VideoPlayerController videoPlayerController = VideoPlayerController.Instance;
     
        public WelcomeUC()
        {
            InitializeComponent();
        }

        public event EventHandler userSelectedEventHandler;

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (txtUserId.Text.Length < 4)
            {
                MessageBox.Show(String.Format("Please enter valid Id"));
            }
            else
            {
                videoPlayerController.user.id = txtUserId.Text;
                userSelectedEventHandler(this, EventArgs.Empty);
            }      
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (videoPlayerController.user.type== UserType.A){
                InfoA.Visibility = System.Windows.Visibility.Visible;
                InfoB.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                InfoA.Visibility = System.Windows.Visibility.Collapsed;
                InfoB.Visibility = System.Windows.Visibility.Visible;
            }
                
        }
            
    }
}

﻿using BicycleProject.Controllers;
using BicycleProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BicycleProject.UserControls
{
    /// <summary>
    /// Interaction logic for TestRound.xaml
    /// </summary>
    public partial class TestRoundUC: UserControl
    {
        VideoPlayerController videoPlayerController = VideoPlayerController.Instance;

        public TestRoundUC()
        {
            InitializeComponent();
        }

        public event EventHandler userContinueEventHandler;

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (videoPlayerController.user.type == UserType.A)
                loadClipsFromDir(@"\MediaA\");
            else
                loadClipsFromDir(@"\MediaB\");


            userContinueEventHandler(this, EventArgs.Empty);
        }

        private void loadClipsFromDir(string path)
        {
            // Process open file dialog box results  
            var localPath = System.IO.Directory.GetCurrentDirectory();

            // load files
            videoPlayerController.loadClipsFromPath(localPath + path);
        }
          
    }
}

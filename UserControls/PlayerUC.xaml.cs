﻿using BicycleProject.Controllers;
using BicycleProject.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;

namespace BicycleProject.UserControls
{
    /// <summary>
    /// Interaction logic for PlayerUC.xaml
    /// </summary>
    public partial class PlayerUC : UserControl, INotifyPropertyChanged
    {
        private int timerDelayInMilSec = 100;   //700
        private int testRounds =4; //4
        
        VideoPlayerController videoPlayerController = VideoPlayerController.Instance;
        Clip currentClip;
        DateTime timeStamp = default(DateTime);

        public PlayerUC()
        {
            

            this.DataContext = this; // bind to self
            InitializeComponent();

        }

        public event EventHandler playerFinishEventHandler;
        public event EventHandler playerFinishTestRound;

        private void displayCountDown()
        {
            var timer1 = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromMilliseconds(timerDelayInMilSec) };
            var timer2 = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromMilliseconds(timerDelayInMilSec) };
            var timer3 = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromMilliseconds(timerDelayInMilSec) }; 
            MediaPlayer.Visibility = System.Windows.Visibility.Hidden;

            setButtonDisplay(false);

            count3.Visibility = System.Windows.Visibility.Visible;
            timer1.Start();

            timer1.Tick += (sender, args) =>
            {
                timer1.Stop();
                count3.Visibility = System.Windows.Visibility.Hidden;
                count2.Visibility = System.Windows.Visibility.Visible;
                timer2.Start();
            };

            timer2.Tick += (sender, args) =>
            {
                timer2.Stop();
                count2.Visibility = System.Windows.Visibility.Hidden;
                count1.Visibility = System.Windows.Visibility.Visible;
                timer3.Start();
            };

            timer3.Tick += (sender, args) =>
            {
                timer3.Stop();
                count1.Visibility = System.Windows.Visibility.Hidden;
                MediaPlayer.Visibility = System.Windows.Visibility.Visible;

                // we want to display "stop" control only if its user A
                if (videoPlayerController.user.type == UserType.A) { typeApanel.Visibility = System.Windows.Visibility.Visible ; }
                timeStamp = DateTime.Now;
                MediaPlayer.Play();
            };            
        }

        private void setButtonDisplay(bool flag)
        {
            // hide grid
            ItemDragGrid.Visibility = System.Windows.Visibility.Hidden;
            // hide panelB until finish/stop
            typeBpanel.Visibility = flag && MediaPlayer.HasVideo ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            // hide/show panelA
            typeApanel.Visibility = flag ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
        }

        private void startPlay()
        {
            // hide panel B
            typeBpanel.Visibility = System.Windows.Visibility.Collapsed;

            if (testRounds-- == 0)
            {
                videoPlayerController.resetList();
                playerFinishTestRound(this, EventArgs.Empty);
            }
            else
            {
                

                // get next clip
                currentClip = videoPlayerController.getNextClip();

                if (currentClip != null)
                {
                    // load clip to media player
                    MediaPlayer.Stop();
                    MediaPlayer.Source = new Uri(currentClip.path);
                    MediaPlayer.Position = TimeSpan.FromSeconds(0);
                    // start countDown
                    displayCountDown();
                }
                else
                {
                    // finish!
                    playerFinishEventHandler(this, EventArgs.Empty);
                }
            }
        }

        private void readClipTimeStanp()
        {
            // calculate play duration
            var totalTime = DateTime.Now - timeStamp;
            //MessageBox.Show(String.Format("User stopped after {0}ms",totalTime.TotalMilliseconds));
            // save clip timespan 
            currentClip.timeSpan = totalTime;

        }

        private void btnSelector(object s, RoutedEventArgs e)
        {
            // re-reroute to B
            if (videoPlayerController.user.type == UserType.B)
                btnPlayB_Click(s, e);
            else
                btnPlayA_Click(s, e);
            
        }

        private void btnPlayA_Click(object sender, RoutedEventArgs e)
        {
            if (btnPlay.Content.ToString() == "Play")
            {
                // save
                saveDraggedImages();

                // start play (and get next clip)
                startPlay();
                btnPlay.Content = "Stop";
            }
            else
            {
                // stop
                MediaPlayer.Stop();

                // set label for next time
                btnPlay.Content = "Play";
                MediaPlayer_MediaEnded(null, null);
                //stopPlay();               
            }
        }

        private void btnPlayB_Click(object sender, RoutedEventArgs e)
        {
            // set string
            btnPlay.Content = "Play";

            // save
            saveDraggedImages();

            // start play (and get next clip)
            startPlay();           
        }

        public void checkVideoPlayerInit()
        {
            // hide player
            MediaPlayer.Visibility = System.Windows.Visibility.Hidden;

            // unload player
            MediaPlayer.Source = null;

            // reset controller typeA content
            btnPlay.Content = "Play";

            // hide panel B force
            typeBpanel.Visibility = System.Windows.Visibility.Collapsed;

            // if A hide all but bicycle
            if (videoPlayerController.user.type == UserType.A)
            {
                truck.Visibility = System.Windows.Visibility.Collapsed;
                bus.Visibility = System.Windows.Visibility.Collapsed;
                car.Visibility = System.Windows.Visibility.Collapsed;
                van.Visibility = System.Windows.Visibility.Collapsed;
                bike.Visibility = System.Windows.Visibility.Collapsed;
                truck.Visibility = System.Windows.Visibility.Collapsed;
            }

            // 
            setButtonDisplay(true);
        }

        private void MediaPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            // reset controller typeA content
            btnPlay.Content = "Play";

            if (sender == null)
                readClipTimeStanp();

            // hide panelB until finish/stop
            //typeBpanel.Visibility = MediaPlayer.HasVideo ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;

            setButtonDisplay(true);

            //if (MediaPlayer.HasVideo)
            //    typeBpanel.Visibility = System.Windows.Visibility.Visible;


            MediaPlayer.Visibility = System.Windows.Visibility.Hidden;
            ItemDragGrid.Visibility = System.Windows.Visibility.Visible;
        }

        private void saveDraggedImages()
        {
            if (currentClip != null && MediaPlayer.HasVideo)
            {
                fillList(1, draggedImages1);
                fillList(2, draggedImages2);
                fillList(3, draggedImages3);
                fillList(4, draggedImages4);
            }
            // reset all checkboxes
            draggedImages1.Clear();
            draggedImages2.Clear();
            draggedImages3.Clear();
            draggedImages4.Clear();
        }

        private void fillList(int q,IEnumerable<Image> list)
        {
            //// collect selected elements
            var listElements = new List<ClipElementsEnum>();

            foreach (var item in list)
            {
                if (item.Source.ToString().Contains("truck"))
                    listElements.Add(ClipElementsEnum.Truck);

                if (item.Source.ToString().Contains("bike"))
                    listElements.Add(ClipElementsEnum.Bike);

                if (item.Source.ToString().Contains("car"))
                    listElements.Add(ClipElementsEnum.Car);

                if (item.Source.ToString().Contains("bicycle"))
                    listElements.Add(ClipElementsEnum.Bicycle);

                if (item.Source.ToString().Contains("van"))
                    listElements.Add(ClipElementsEnum.Van);

                if (item.Source.ToString().Contains("bus"))
                    listElements.Add(ClipElementsEnum.Bus);
            }

            currentClip.elements.Add(new ClipElements()
            {
                qouter = q,
                elements = listElements,
            });
        }

        ///////////////////////////////////////////////////////////////////////////// observable collection
        private ObservableCollection<Image> draggedImages1 = new ObservableCollection<Image>();
        public ObservableCollection<Image> DraggedImages1
        {
            get { return draggedImages1; }
        }
        private ObservableCollection<Image> draggedImages2 = new ObservableCollection<Image>();
        public ObservableCollection<Image> DraggedImages2
        {
            get { return draggedImages2; }
        }
        private ObservableCollection<Image> draggedImages3 = new ObservableCollection<Image>();
        public ObservableCollection<Image> DraggedImages3
        {
            get { return draggedImages3; }
        }
        private ObservableCollection<Image> draggedImages4 = new ObservableCollection<Image>();
        public ObservableCollection<Image> DraggedImages4
        {
            get { return draggedImages4; }
        }

        private void ListView_Drop(object sender, DragEventArgs e)
        {
            //
            ImageSource image = e.Data.GetData(typeof(ImageSource)) as ImageSource;

            Image imageControl = new Image() { Width = 100, Height = 100, Source = image };

            switch ((sender as ListView).Name)
            {
                case "ListView1":
                    draggedImages1.Add(imageControl);
                    break;
                case "ListView2":
                    draggedImages2.Add(imageControl);
                    break;
                case "ListView3":
                    draggedImages3.Add(imageControl);
                    break;
                case "ListView4":
                    draggedImages4.Add(imageControl);
                    break;
            }

            // reset border
            ListView_DragLeave(sender, null);
            
        }

        private void ListView_DragEnter(object sender, DragEventArgs e)
        {
            (sender as ListView).BorderBrush = Brushes.Red;
            (sender as ListView).BorderThickness = new Thickness(2);
        }

        private void ListView_DragLeave(object sender, DragEventArgs e)
        {
            (sender as ListView).BorderBrush = Brushes.Black;
            (sender as ListView).BorderThickness = new Thickness(1);
        }

        private void img_MouseMove(object sender, MouseEventArgs e)
        {
            Image image = sender as Image;
            if (image != null && e.LeftButton == MouseButtonState.Pressed)
            {
                DataObject data = new DataObject(typeof(ImageSource), image.Source);
                DragDrop.DoDragDrop(image, data, DragDropEffects.Copy);
            }
        }

        
        /// ////////////////////////////////////////////////////////////////////////////////////////
        
        public void HandleHandledKeyDown(object sender, RoutedEventArgs e)
        {
            if (this.Visibility == System.Windows.Visibility.Visible)
            {
                KeyEventArgs ke = e as KeyEventArgs;
                if (ke.Key == Key.Space || ke.Key == Key.Enter)
                {
                    if (MediaPlayer.Visibility == System.Windows.Visibility.Visible)
                        btnSelector(null, null);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);

            // set event handler
            window.KeyDown += HandleHandledKeyDown;
            //this.Focusable = true;
            //this.Focus();
        } 
    }
}

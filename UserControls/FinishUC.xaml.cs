﻿using BicycleProject.Controllers;
using BicycleProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BicycleProject.UserControls
{
    /// <summary>
    /// Interaction logic for FinishUC.xaml
    /// </summary>
    public partial class FinishUC : UserControl
    {
        VideoPlayerController videoPlayerController = VideoPlayerController.Instance;
     
        public FinishUC()
        {
            InitializeComponent();

        }


        private void btnOpenBrowser_Click(object sender, RoutedEventArgs e)
        {
            var address = @"https://docs.google.com/forms/d/1q7yVYsiaNz3VuahSqUFjaWcZvtA33WZlqqBu-T8U7ko/viewform";

            // open browser
            System.Diagnostics.Process.Start(address);

            // close application
            Application.Current.Shutdown();

        }
        

            
    }
}
